# README #

## Overview ##

This repository contains a Bash implementation of the SOS Proxy, a script that sends O&M data files (encoded in XML or EXI) to a SOS server. The main purpose of this software is to provide transparent access to a SOS server, hiding the http transactions from underlying software.

## Configuration ##

The SOS Proxy is configured using a configuration file (an example is provided), where the server URL, acquisition paths and all the required variables are included. 

