#!/bin/bash
#===================================================================#
#                       SOS Proxy script                            #
#===================================================================#
# This script searches for files in "ACQUISITION_DATA_PATH" and     #
# sends it to a SOS server. Afterwards the sent files are moved     #
# to the "destination_path", organizing them in folders by year,    #
# month and day:                                                    #
#                                                                   #
# dest_path/year/month/day/file                                     #
#===================================================================#
# @author : Enoc Martínez                                           #
# @contact : enoc.martinez@upc.edu                                  #
# @organization : Universitat Politécnica de Catalunya    (UPC)     #
#                                                                   #
#===================================================================#



#----------- Colour codes ------------#
# Colour codes, for internal use only, do not modify
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'
#---------------------------------------------------------------------------#
#--------------------------- FUNCTION DEFINITIONS --------------------------#
#---------------------------------------------------------------------------#

#-------------------------------------------------------------------#
#-------------------------------- msg ------------------------------#
#-------------------------------------------------------------------#
# This function echoes its argument and, at the same time, stores   #
# them to the log file. the log filename has to be defined          #
#-------------------------------------------------------------------#
msg(){
    printf "$1\n"
    printf "$1\n" >> $log
}


#-------------------------------------------------------------------#
#-------------------------- updateLogName --------------------------#
#-------------------------------------------------------------------#
# This function updates the log name with the date:                 #
# LOG_PATH/YEAR/MONTH/sos_proxy_DAY.log                             #
#-------------------------------------------------------------------#
updateLogName(){
    year=$(date +%Y)    
    month=$(date +%m)

    if [ ! -e $LOG_PATH/$year/$month ]; then
        echo "creating new directory $log_path/$year/$month... "
        mkdir -p $LOG_PATH/$year/$month  -m 775
    fi

    log=$LOG_PATH/$year/$month/$log_name$(date '+%d-%m-%y').log     
}

sendfile(){
    # arg 1 : file
    # arg 2 : URL
    # arg 3 : format (xml / exi)
    
    wget --no-check-certificate --output-file=$http_response \
            --output-document=$server_response \
            --header="Content-type: application/$3" \
            --post-file $1 $2
}


#---------------------------------------------------------------------------#
#------------------------------ PROGRAM START ------------------------------#
#---------------------------------------------------------------------------#

if [ $# != 1 ]; then
    echo "usage: $0 <conf file>"
    exit
fi

if [ ! -f $1  ]; then
    echo "${red}file $1 does not exist!${end}"
    exit
fi


source $1
printf "\n\n"
printf "${grn}=====================================${end}\n"
printf "${grn}======== Starting SOS Proxy =========${end}\n"
printf "${grn}=====================================${end}\n\n"

log="" #declare the log variable
echo "${blu}============ Setup =============${end}"

# if the destination path does not exist
if [ ! -e $ACQUISITION_DATA_PATH ]; then
    echo "source path $ACQUISITION_DATA_PATH does not exist, exit"
    exit 0
else 
    echo "Moving to execution path:"
    echo $ACQUISITION_DATA_PATH
    cd $ACQUISITION_DATA_PATH
fi

# if the destination path does not exist
if [ ! -e $RAW_DATA_PATH ]; then
    mkdir -p $RAW_DATA_PATH
    echo "Creating a new dir for raw data"
fi

echo "Storing files in:"
echo "$RAW_DATA_PATH"

# if the destination path does not exist
if [ ! -e $LOG_PATH ]; then
    mkdir -p $LOG_PATH
fi

echo "Storing logs files in:"
echo  $LOG_PATH
echo "${blu}==== Starting infinite loop ====${end}"


while : ; do

    updateLogName    #update the log name with the date


    for file in $ACQUISITION_DATA_PATH/*.xml ; do
        if [ -e $file ]; then
            # Send the file to the SOS server

            msg "\n--------------------------------------------------------------------"
            date_str=$(date)
            msg "$date_str"

            #send the file to the SOS server
            msg "Sending file $file to $SOS_XML_URL"                
            sendfile $file $SOS_XML_URL "xml"

            if [ -e $http_response ]; then
                cat $http_response | grep HTTP
                cat $http_response | grep HTTP >> $log
                rm $http_response
            fi

            if [ -e $server_response ]; then    
                cat $server_response | grep Exception
                cat $server_response | grep Exception >> $log
                rm $server_response
            fi

            # Move the file to dest_path/year/month/day/file.xml

            year=$(date +%Y)
            month=$(date +%m)
            day=$(date +%d)
            if [ ! -e $RAW_DATA_PATH/$year/$month/$day ]; then
                echo "creating new directory $RAW_DATA_PATH/$year/$month/$day..."
                mkdir -p $RAW_DATA_PATH/$year/$month/$day -m 775
            fi
    
            chmod 664 $file
            msg "moving file $file to $RAW_DATA_PATH/$year/$month/$day"
            mv $file $RAW_DATA_PATH/$year/$month/$day
        
            sleep 0.1        
        fi
    done

    for file in $ACQUISITION_DATA_PATH/*.exi ; do
        if [ -e $file ]; then
            # Send the file to the SOS server

            msg "\n--------------------------------------------------------------------"
            date_str=$(date)
            msg "$date_str"

            #send the file to the SOS server
            msg "Sending file $file to $SOS_EXI_URL"                
            sendfile $file $SOS_EXI_URL "exi"

            if [ -e $http_response ]; then
                cat $http_response | grep HTTP
                cat $http_response | grep HTTP >> $log
                rm $http_response
            fi

            if [ -e $server_response ]; then    
                rm $server_response
            fi

            # Move the file to dest_path/year/month/day/file.xml

            year=$(date +%Y)
            month=$(date +%m)
            day=$(date +%d)
            if [ ! -e $RAW_DATA_PATH/$year/$month/$day ]; then
                echo "creating new directory $RAW_DATA_PATH/$year/$month/$day..."
                mkdir -p $RAW_DATA_PATH/$year/$month/$day -m 775
            fi
    
            chmod 664 $file
            msg "moving file $file to $RAW_DATA_PATH/$year/$month/$day"
            mv $file $RAW_DATA_PATH/$year/$month/$day
        
            sleep 0.1        
        fi
    done

    echo "sleeping 10 s..."
    sleep 10
done
exit

